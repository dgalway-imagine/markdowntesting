# markdowntesting repository

This repo is a spot to muck around with what works and does not work in terms of Bitbucket markdown.

- - - - 
# What did I learn about Markdown while getting this to work?

- You must linefeed before the beginning of a list  
- You must put two spaces after the end of a line to get a linefeed in Markdown  
- You must insert a space between hash-marks and the heading when creating Heading styles.  
- Markdown is picky and unnatural  
- Windows line endings are fine.. didn't need to use UNIX LF in the end.  
- Block-of-Code markdown:  
	- Requires a preceding linefeed.  
	- Must be intended by a tab (or 4 spaced) or fenced with tilde  
	- Will expose tabbing if there is a second tab.  Thus, one tab "makes it code" and the second tab "tabs the code".  
	


- - - - 
## Scratch Pad:

A list of items.. hopefully:  

- Updating the RunAs administrative user of the ConductorStartupTasks\ConductorStartup scheduled task to AdminUsername as specified in the Versio configuration.  
- Creating desktop shortcuts "Start Conductor" and "Stop Conductor" including connecting the shortcuts to the back-end scripts that implement them.  
	- indented list item.  

### Required Subheading H3:
AdminUsername

### Dependencies Subheading H3:
one option  
two options

### A Table:
Code on this branch is built by:  

Parameter | Value
------------ | ----------------
Build Server | build.wtldev.net
Job Name | PL_Chocolatey\imagine-common
URL | ~~https://build.wtldev.net/view/Bitbucket.org/job/PL_Chocolatey/job/imagine-common/ ~~  


### Code Markdown:
Tag format:  

		v[Major].[Minor].[Patch]  

If the tag is on the tip the job will build that version.  
If the version is below the tip you will get `v[Major].[Minor].[Patch+1]`  


## A note about repo history:
2021-Aug-24 DGALWAY:
This README was modified in 2018 to indicate this repo is [DEPRECATED] and "only here for reference".

> *--- ORIGINAL Text of README.MD ----*  
> This is a historical note for reference of the future of person-kind.  
> This repository is only here for reference.  
> **Do not use for any new development**.  



> *---- Start ORIGINAL Text of README.MD ----*  
> # conductor-config repository [DEPRECATED]  
>  
> This package was part of the PackagingSetup deployment and has been replaced by the [win_magellan_diagnostic_logging](http://git.myimagine.com/projects/DEP/repos/versio/browse/ansible/roles/win_conductor) Ansible role.  
>  
> This repository is only here for reference, **do not use for any new development**.  
 
 
 

